import { Type } from "class-transformer";

export class Signup{
name:string;
password:string;
@Type(() => Address)
address:Address;
}

export class Address{
 addressline1:string;
 addressline2:string;
 addressline3:string;
 addressline4:string;   
}