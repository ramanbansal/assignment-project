import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { Signup, Address } from './sign';
import * as $ from 'jquery';
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    signData: Signup;
    public file;
    signList = [];
    addressList = [];
    message: string;
    constructor() { }

    ngOnInit() {
        this.signData = new Signup();
        this.signData.address = new Address();
        this.closePop();
        
    }
showPop(){
const mod=document.getElementById('modal');
mod.style.display="block";
}
closePop(){
    const mod=document.getElementById('modal');
    mod.style.display="none";
    }
    
    onFileChanged(event) {
         this.file = event.target.files[0];
    }
        
    imgpic(){
         console.log(this.file);
         
         this.closePop();
         // Take action when the image has loaded
          this.file.addEventListener("load", function () {
         var imgCanvas = document.createElement("canvas"),
             imgContext = imgCanvas.getContext("2d");
         
         // Make sure canvas is as big as the picture
         imgCanvas.width = this.file.width;
         imgCanvas.height = this.file.height;
         
         // Draw image into canvas element
         imgContext.drawImage(this.file, 0, 0, this.file.width, this.file.height);
         
         // Get canvas contents as a data URL
         var imgAsDataURL = imgCanvas.toDataURL("image/png");
         
         // Save image into localStorage
         try {
             localStorage.setItem("Image", imgAsDataURL);
         }
         catch (e) {
             console.log("Storage failed: " + e);
         }
         }, false); 
         
         
       
    
        }


    register() {
        this.signList.push(this.signData);
        this.storeSign(JSON.stringify(this.signList));
        this.message = "Registered successfully";
    }

    storeSign(signdata) {
        localStorage.setItem("loginData", signdata);

    }

}
