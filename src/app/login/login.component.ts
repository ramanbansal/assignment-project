import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { login } from './data';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    logindata: login;
    allSignData = [];
    message: string;
    dataLi = [
        { "id": "1", "name": "Raman", "mobile": "8606545554", "email": "raman@example.com" },
        { "id": "2", "name": "rakesh", "mobile": "8606545554", "email": "raman@example.com" },
        { "id": "3", "name": "aman", "mobile": "8606545554", "email": "raman@example.com" },
        { "id": "4", "name": "ankush", "mobile": "8606545554", "email": "raman@example.com" }
    ];

    constructor(
        public router: Router
    ) { }

    ngOnInit() {
        this.logindata = new login();
        this.allSignData = JSON.parse(this.getSignData());
    }

    onLoggedin() {
        if (this.allSignData == null || this.allSignData == undefined) {
            this.message = "Incorrect username or password";
        } else {
            for (let i = 0; i < this.allSignData.length; i++)
                if (this.logindata.name == this.allSignData[i].name && this.logindata.password == this.allSignData[i].password) {
                    localStorage.setItem('isLoggedin', 'true');
                    this.router.navigate(["/dashboard"]);
                    this.storeData(JSON.stringify(this.dataLi));
                }
                else {
                    this.message = "Incorrect username or password";
                }
        }
    }


    getData() {
        return localStorage.getItem("data");

    }
    getSignData() {
        return localStorage.getItem("loginData");

    }
    removeData() {
        return localStorage.removeItem("data");
    }

    storeData(datalist) {
        localStorage.setItem("data", datalist);

    }
}
